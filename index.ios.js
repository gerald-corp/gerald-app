import { AppRegistry } from 'react-native';

import GeraldApp from './app/index';

AppRegistry.registerComponent('GeraldApp', () => GeraldApp);

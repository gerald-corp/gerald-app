export const RESIZE_WINDOW = 'RESIZE_WINDOW';
export const UPDATE_FORM = 'UPDATE_FORM';
export const SUBMIT_FORM = 'SUBMIT_FORM';
export const UNSUBMIT_FORM = 'UNSUBMIT_FORM';
export const DELETE_FORM = 'DELETE_FORM';

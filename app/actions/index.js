import * as types from './types';

export const resizeWindow = (width, height) => ({
  type: types.RESIZE_WINDOW,
  width,
  height,
});

export const updateForm = (formName, kv) => ({
  type: types.UPDATE_FORM,
  formName,
  kv,
});

export const submitForm = (formName) => ({
  type: types.SUBMIT_FORM,
  formName,
});

export const unsubmitForm = (formName) => ({
  type: types.UNSUBMIT_FORM,
  formName,
});

export const deleteForm = (formName) => ({
  type: types.DELETE_FORM,
  formName,
});

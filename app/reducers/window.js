import { RESIZE_WINDOW } from '../actions/types';
import { PSD_WIDTH, PSD_HEIGHT, PSD_RATIO } from '../constants/dimensions';


const initialState = {
  width: 0,
  height: 0,
  widthRatio: 0,
  heightRatio: 0,
};

const window = (state = initialState, action) => {
  switch (action.type) {
    case RESIZE_WINDOW: {
      const [ width, height ] = (() => {
        const { width, height } = action;
        if (width / height > 3/4) {
          return [ height * PSD_RATIO, height ];
        }
        return [ width, height ];
      })();
      const widthRatio = width / PSD_WIDTH;
      const heightRatio = height / PSD_HEIGHT;

      return { width, height, widthRatio, heightRatio };
    }

    default: {
      return state;
    }
  }
};

export default window;

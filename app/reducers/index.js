import { combineReducers } from 'redux';
import window from './window';
import forms from './forms';

const reducer = combineReducers({
  window,
  forms,
});

export default reducer;

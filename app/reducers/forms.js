import {
  UPDATE_FORM,
  SUBMIT_FORM,
  UNSUBMIT_FORM,
  DELETE_FORM,
} from '../actions/types';

function updateForm(state = {}, action) {
  switch(action.type) {
    case UPDATE_FORM: {
      const { formName, kv } = action;
      let nextState;
      if (state[formName] !== undefined) {
        const form = state[formName];
        nextState = {
          ...state,
          [formName]: {
            submitted: form.submitted,
            data: {
              ...form.data,
              ...kv,
            },
          },
        };
      } else {
        nextState = {
          ...state,
          [formName]: {
            submitted: false,
            data: {
              ...kv,
            },
          },
        };
      }

      return nextState;
    }

    default: {
      return state;
    }
  }
}

function submitForm(state = {}, action) {
  switch(action.type) {
    case SUBMIT_FORM: {
      const { formName } = action;
      let nextState;
      if (state[formName] !== undefined) {
        const form = state[formName];
        nextState = {
          ...state,
          [formName]: {
            submitted: true,
            data: { ...form.data },
          },
        };
      } else {
        nextState = state;
      }

      return nextState;
    }

    default: {
      return state;
    }
  }
}

function unsubmitForm(state = {}, action) {
  switch(action.type) {
    case UNSUBMIT_FORM: {
      const { formName } = action;
      let nextState;
      if (state[formName] !== undefined) {
        const form = state[formName];
        nextState = {
          ...state,
          [formName]: {
            submitted: false,
            data: { ...form.data },
          },
        };
      } else {
        nextState = state;
      }

      return nextState;
    }

    default: {
      return state;
    }
  }
}

function deleteForm(state = {}, action) {
  switch(action.type) {
    case DELETE_FORM: {
      const { formName } = action;
      let nextState;
      if (state[formName] !== undefined) {
        nextState = Object.keys(state)
          .filter(key => key !== formName)
          .reduce((obj, key) => {
            obj[key] = state[key];
            return obj;
          }, {});
      } else {
        nextState = state;
      }

      return nextState;
    }

    default: {
      return state;
    }
  }
}

export default function forms(state = {}, action) {
  switch(action.type) {
    case UPDATE_FORM: {
      return updateForm(state, action);
    }

    case SUBMIT_FORM: {
      return submitForm(state, action);
    }

    case UNSUBMIT_FORM: {
      return unsubmitForm(state, action);
    }

    case DELETE_FORM: {
      return deleteForm(state, action);
    }

    default: {
      return state;
    }
  }
}

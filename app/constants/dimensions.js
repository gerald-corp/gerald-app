export const PSD_WIDTH = 640;
export const PSD_HEIGHT = 1096;
export const PSD_DIAGONAL = Math.sqrt((PSD_WIDTH*PSD_WIDTH) + (PSD_HEIGHT*PSD_HEIGHT));
export const PSD_RATIO = PSD_WIDTH / PSD_HEIGHT;
export const WINDOW_WIDTH = Number.MAX_VALUE;
export const WINDOW_HEIGHT = Number.MAX_VALUE;

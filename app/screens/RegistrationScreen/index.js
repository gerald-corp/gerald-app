import React, { Component } from 'react';
import {
  Alert,
  AsyncStorage,
  Image,
  Keyboard,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import TextInputState from 'react-native/lib/TextInputState';
import { connect } from 'react-redux';
import { graphql, gql } from 'react-apollo';

import Form from '../../components/Form';
import RadioButtonsGroup from '../../components/RadioButtonsGroup';
import RadioButton from '../../components/RadioButton';
import TextInput from '../../components/TextInput';
import DateInput from '../../components/DateInput';
import CheckBox from '../../components/CheckBox';
import Button from '../../components/Button';

import { submitForm, unsubmitForm } from '../../actions';
import { createStyleSheet } from '../../functions/styles';
import { WINDOW_WIDTH, WINDOW_HEIGHT } from '../../constants/dimensions';


const styles = {
  container: {
    flex: 1,
  },

  backgroundImage: {
    flex: 1,
    width: undefined,
    height: undefined,
  },

  backgroundMask: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },

  header: {
    width: '100%',
    height: '22.99270072992701%',
    borderBottomWidth: 3,
    borderBottomColor: 'black',
  },

  backButton: {
    position: 'absolute',
  },

  backButtonContainer: {
    width: 73,
    height: 83,
    justifyContent: 'center',
    alignItems: 'center',
  },

  backButtonIcon: {
    width: 25,
    height: 39,
  },

  shadow: {
    position: 'absolute',
    top: 57,
    left: 544,
    width: 96,
    height: 192,
  },

  illustration: {
    position: 'absolute',
    top: 9,
    left: 455,
    width: 128,
    height: 240,
  },

  title: {
    position: 'absolute',
    top: 183,
    left: 19,
    fontFamily: 'Galada-Regular',
    fontSize: 48,
    includeFontPadding: false,
    textAlignVertical: 'center',
    color: 'black',
  },

  form: {
    flex: 1,
    width: '100%',
    height: '77.00729927007299%',
    justifyContent: 'space-between',
  },

  inputsContainer: {
    flex: 1,
    paddingTop: 30,
    paddingLeft: 18,
    paddingRight: 18,
  },

  inputs: {
    width: '100%',
    height: 655,
    justifyContent: 'space-between',
  },

  radioButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 200,
  },

  textInput: {
    // marginTop: 19,
  },

  checkbox: {
    // marginTop: 19,
  },

  cguText: {
    fontFamily: 'Quicksand-Bold',
    fontSize: 22,
    color: '#666666ff',
  },

  button: {
    width: '100%',
    height: 70,
    backgroundColor: '#f1c500ff',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

class RegistrationScreen extends Component {
  constructor(props) {
    super(props);

    this.fields = [ 'email', 'password', 'confirmPassword', 'gender',
                    'firstName', 'lastName', 'birthDate', 'phone' ];

    const { width, height } = props.window;
    this.state = {
      width,
      height,
      styles: createStyleSheet(styles, width, height),
      fieldsErrors: {},
    };
  }

  componentWillReceiveProps(nextProps) {
    const { width, height } = nextProps.window;
    this.setState({
      width,
      height,
      styles: createStyleSheet(styles, width, height),
    });
  }

  componentWillMount () {
    // this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentWillUnmount () {
    // this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  // _keyboardDidShow () {
  //   alert('Keyboard Shown');
  // }

  _keyboardDidHide () {
    TextInputState.blurTextInput(TextInputState.currentlyFocusedField());
    // alert('Keyboard Hidden');
  }

  handleSubmit(data) {
    if (!this.validateFields()) {
      this.props.unsubmitForm('inscription');
    } else {
      this.props.createUser({ variables: data })
        .then((response) => {
          this.props.signinUser({ variables: { email: data.email, password: data.password } })
            .then((response) => {
              const token = response.data.signinUser.token;
              AsyncStorage.setItem('@GeraldStore:authToken', token);
              Alert.alert('Inscription réussie', 'Vous êtes maintenant inscrits!');
              this.props.unsubmitForm('inscription');
            });
        }).catch((e) => {
          if (e.message.includes('already exists')) {
            Alert.alert("Erreur d'inscription", "Un autre utilisateur s'est déja inscrit avec cette adresse email.");
            this.props.unsubmitForm('inscription');
          }
        });
    }
  }

  validateFields() {
    const { forms } = this.props;
    const form = forms.inscription || { data: {} };
    const formData = form.data;
    const errors = {};

    for (let i = 0; i < this.fields.length; i++) {
      const field = this.fields[i];
      const value = formData[field];
      if (value === undefined || value === '') {
        let error;
        if (field === 'gender') {
          error = 'Choisissez une option.';
        } else {
          error = 'Ce champs est requis, vous devez le remplir.';
        }
        errors[field] = error;
      }
    };

    if (errors['email'] === undefined && !this.validateEmail(formData['email'])) {
      errors['email'] = 'Adresse email invalide.';
    }

    if (errors['phone'] === undefined && !this.validatePhoneNumber(formData['phone'])) {
      errors['phone'] = 'Numéro de téléphone invalide.';
    }

    if (errors['confirmPassword'] === undefined && formData.password !== formData.confirmPassword) {
      errors['confirmPassword'] = 'Les mots de passe ne sont pas identiques.';
    }

    if (!formData.acceptCGU) {
      errors['acceptCGU'] = 'Vous devez accepter les CGU afin de vous inscrire.';
    }

    this.setState({ fieldsErrors: errors });

    return Object.keys(errors).length === 0;
  }

  validateEmail(email) {
    const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return regex.test(email);
  }

  validatePhoneNumber(number) {
    const regex = /^\+?\s*(\d+\s?){8,}$/;
    return regex.test(number);
  }

  render() {
    const { styles, width, height, fieldsErrors } = this.state;

    return (
      <View style={styles.container}>
        <Image
          source={require('./images/background.png')}
          style={styles.backgroundImage}
          resizeMode={'stretch'}
        >
          <View style={styles.backgroundMask}>
            <View style={styles.header}>
              <View style={styles.backButton}>
                <TouchableWithoutFeedback onPress={() => this.props.navigation.goBack(null)}>
                  <View style={styles.backButtonContainer}>
                    <Image
                      style={styles.backButtonIcon}
                      source={require('./images/back-button.png')}
                      />
                  </View>
                </TouchableWithoutFeedback>
              </View>

              <Text allowFontScaling={false} style={styles.title}>
                Bonjour ! Inscrivez-vous
              </Text>
            </View>

            <Form style={styles.form} name="inscription" onSubmit={this.handleSubmit.bind(this)}>
              <View style={styles.inputsContainer}>
                <View style={styles.inputs}>
                  <RadioButtonsGroup style={styles.radioButtons} name="gender" error={fieldsErrors['gender']}>
                    <RadioButton value="male" text="M." />
                    <RadioButton value="female" text="Mme" />
                  </RadioButtonsGroup>

                  <TextInput name="firstName" style={styles.textInput} placeholder="Prénom" autoCapitalize="words" error={fieldsErrors['firstName']} />
                  <TextInput name="lastName" style={styles.textInput} placeholder="Nom" autoCapitalize="words" error={fieldsErrors['lastName']} />
                  <DateInput name="birthDate" style={styles.textInput} placeholder="Date de naissance" error={fieldsErrors['birthDate']} />
                  <TextInput name="email" style={styles.textInput} placeholder="Adresse e-mail" kayboardType="email-address" error={fieldsErrors['email']} />
                  <TextInput name="phone" style={styles.textInput} placeholder="Numéro de téléphone" keyboardType="phone-pad" error={fieldsErrors['phone']} />
                  <TextInput name="password" secureTextEntry={true} style={styles.textInput} placeholder="Mot de passe" error={fieldsErrors['password']} />
                  <TextInput name="confirmPassword" secureTextEntry={true} style={styles.textInput} placeholder="Confirmer le mot de passe" error={fieldsErrors['confirmPassword']} />

                  <CheckBox name="acceptCGU" style={styles.checkbox} error={fieldsErrors['acceptCGU']}>
                    <Text style={styles.cguText}>
                      J'accepte les CGU de Gérald
                    </Text>
                  </CheckBox>
                </View>
              </View>

              <Button style={styles.button} value="C'est parti !" onPress={() => this.props.submitForm('inscription')}/>
            </Form>
          </View>
        </Image>
      </View>
    );
  }
}

const createUser = gql`
  mutation (
    $email: String!,
    $password: String!,
    $gender: String!,
    $firstName: String!,
    $lastName: String!,
    $birthDate: String!,
    $phone: String!,
  ) {
    createUser(
      authProvider: {
        email: { email: $email, password: $password }
      },
      gender: $gender,
      firstName: $firstName,
      lastName: $lastName,
      birthDate: $birthDate,
      phone: $phone,
    ) {
      id
    }
  }
`;

const signinUser = gql`
  mutation ($email: String!, $password: String!) {
    signinUser(email: {email: $email, password: $password}) {
      token
    }
  }
`;

const userQuery = gql`
  query {
    user {
      id
    }
  }
`;

const mapStateToProps = state => ({
  window: state.window,
  forms: state.forms,
});

const container = connect(mapStateToProps, { submitForm, unsubmitForm })(RegistrationScreen);

export default graphql(createUser, { name: 'createUser' })(
  graphql(userQuery, { options: { fetchPolicy: 'network-only' }})(
    graphql(signinUser, { name: 'signinUser' })(
      container
    )
  )
);

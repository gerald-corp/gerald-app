import React, { Component } from 'react';
import {
  Dimensions,
  Image,
  PixelRatio,
  Text,
  StatusBar,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { connect } from 'react-redux';

import SlideShow from './components/SlideShow';
import Button from '../../components/Button';

import { createStyleSheet } from '../../functions/styles';
import { getHSize, getVSize } from '../../functions/dimensions';
import { WINDOW_WIDTH, WINDOW_HEIGHT } from '../../constants/dimensions';


const styles = {
  container: {
    width: WINDOW_WIDTH,
    height: WINDOW_HEIGHT,
    backgroundColor: '#fff',
  },

  contentContainer: {
    width: WINDOW_WIDTH,
    height: WINDOW_HEIGHT,
    alignItems: 'center',
  },

  logo: {
    width: 284,
    height_h: 284,
    top: 20,
  },

  buttonsContainer: {
    position: 'absolute',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 440,
    height: 64,
    top: 912,
  },

  button: {
    width: 205,
    height: '100%',
    backgroundColor: '#f1c500ff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonText: {
    fontSize: 24,
    top: -3,
  },

  helpLink: {
    flexDirection: 'row',
    position: 'absolute',
    top: 1040,
    justifyContent: 'center',
    alignItems: 'center',
  },

  helpText: {
    fontFamily: 'Quicksand-Bold',
    fontSize: 32,
    includeFontPadding: false,
    textAlignVertical: 'center',
    color: '#fff',
    top: -3,
  },

  helpTextGoSymbol: {
    marginLeft: 13,
    width: Math.sqrt(200),
    height: Math.sqrt(200),
    borderTopWidth: 4,
    borderRightWidth: 4,
    borderColor: '#fff',
    transform: [{ rotate: '45deg' }],
  },
};

class HomeScreen extends Component {
  constructor(props) {
    super(props);

    const { width, height, widthRatio, heightRatio } = props.window;
    this.state = {
      width,
      height,
      widthRatio,
      heightRatio,
      styles: createStyleSheet(styles, width, height)
    };
  }

  componentWillReceiveProps(nextProps) {
    const { width, height, widthRatio, heightRatio } = nextProps.window;
    this.setState({
      width,
      height,
      widthRatio,
      heightRatio,
      styles: createStyleSheet(styles, width, height)
    });
  }

  render() {
    const { styles, width, widthRatio, heightRatio } = this.state;
    const { navigation } = this.props;
    const images = [
      {
        source: require('./images/babysitting1.png'),
        width: getHSize(640, widthRatio),
        height: getVSize(1096, heightRatio),
        topLeft: [ 0, 0 ],
      },
      {
        source: require('./images/menage.png'),
        width: getHSize(640, widthRatio),
        height: getVSize(1096, heightRatio),
        topLeft: [ 0, 0 ],
      },
      {
        source: require('./images/petsitting.png'),
        width: getHSize(640, widthRatio),
        height: getVSize(1096, heightRatio),
        topLeft: [ 0, 0 ],
      },
      {
        source: require('./images/babysitting2.png'),
        width: getHSize(640, widthRatio),
        height: getVSize(1096, heightRatio),
        topLeft: [ 0, 0 ],
      },
      {
        source: require('./images/cuisine.png'),
        width: getHSize(640, widthRatio),
        height: getVSize(1096, heightRatio),
        topLeft: [ 0, 0 ],
      },
    ];

    return (
      <View style={styles.container}>
        <SlideShow images={images} width={width} timing={3000}>
          <View style={styles.contentContainer}>
            <Image
              source={require('./images/logo-transparent.png')}
              style={styles.logo}
              />

            <View style={styles.buttonsContainer}>
              <Button
                style={styles.button}
                textStyle={styles.buttonText}
                value="Connexion"
                onPress={() => navigation.navigate('Login')}
                />

              <Button
                style={styles.button}
                textStyle={styles.buttonText}
                value="Inscription"
                onPress={() => navigation.navigate('Registration')}
                />
            </View>

            <View style={styles.helpLink}>
              <Text allowFontScaling={false} style={styles.helpText}>
                Comment ça marche ?
              </Text>
              <View style={styles.helpTextGoSymbol} />
            </View>
          </View>
        </SlideShow>
      </View>
    );
  }
};

const mapStateToProps = state => ({
  window: state.window,
});

export default connect(mapStateToProps)(HomeScreen);

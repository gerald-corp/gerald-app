import React, { Component } from 'react';
import {
  Animated,
  Image,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  slide: {
    flex: 1,
    width: '100%',
    height: '100%',
    position: 'absolute',
  },

  image: {
    flex: 1,
    width: undefined,
    height: undefined,
  },

  childrenContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
});

class SlideShow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images: props.images,
      numberOfSlides: props.images.length,
      timer: null,
      translation: new Animated.Value(0),
      translateX: 0,
    };

    this.addTranslationListener(this.state.translation);
  }

  addTranslationListener(translation) {
    translation.addListener(({ value }) => {
      this.setState({ translateX: value });
    });
  }

  // gotoSlide(index) {
  //   const { currentSlideIndex, numberOfSlides, translation } = this.state;
  //   let nextTranslation;

  //   if (index < 0 || index >= numberOfSlides || index === currentSlideIndex) {
  //     return;
  //   }

  //   if (index > currentSlideIndex) {
  //     nextTranslation = translation - this.props.width;
  //   } else {
  //     nextTranslation = translation + this.props.width;
  //   }

  //   Animated.timing(
  //     this.state.translation,
  //     { toValue: nextTranslation },
  //   ).start();
  // }

  autoSlide() {
    const timer = setTimeout(() => {
      this.nextSlide(() => this.autoSlide());
    }, this.props.timing);

    this.setState({ timer });
  }

  nextSlide(callback) {
    const slideShowWidth = this.props.width;

    if (this.state.numberOfSlides > 1) {
      Animated.timing(
        this.state.translation,
        // { toValue: -slideShowWidth },
        { toValue: 1, duration: 700 },
      ).start(({ finished }) => {
        if (finished) {
          const nextImagesArray = this.rotateArray(this.state.images);
          const translation = new Animated.Value(0);
          this.addTranslationListener(translation);

          this.setState({
            images: nextImagesArray,
            translateX: 0,
            translation,
          });
          callback();
        }
      });
    }
  }

  rotateArray(arr) {
    const a = arr.slice();
    const head = a.shift();
    a.push(head);
    return a;
  }

  componentDidMount() {
    SplashScreen.hide();
    this.autoSlide();
  }

  componentWillUnmout() {
    clearTimeout(this.state.timer);
  }

  render() {
    const { images, translateX } = this.state;
    const { width } = this.props;
    const opacity = [ 1, 1 - translateX ];

    return (
      <View style={StyleSheet.flatten([
        styles.container,
        { width }
      ])}>
        {images.slice(0, 2).reverse().map((image, i) => (
          <View
            key={i.toString()}
            style={StyleSheet.flatten([
              styles.slide,
              // {
                // left: width * i,
                // transform: [
                //   { translateX },
                // ],
              // },
            ])}
            >
            <Image
              source={image.source}
              style={StyleSheet.flatten([
                styles.image,
                { opacity: opacity[i] },
              ])}
              resizeMode={Image.resizeMode.cover}
              />
          </View>
        ))}
        <View style={styles.childrenContainer}>
        {this.props.children}
      </View>
      </View>
    );
  }
}

export default SlideShow;

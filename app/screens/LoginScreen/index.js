import React, { Component } from 'react';
import {
  Alert,
  AsyncStorage,
  Image,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import { graphql, gql } from 'react-apollo';

import Form from '../../components/Form';
import TextInput from '../../components/TextInput';
import Button from '../../components/Button';

import { submitForm, unsubmitForm } from '../../actions';
import { createStyleSheet } from '../../functions/styles';
import { WINDOW_WIDTH, WINDOW_HEIGHT } from '../../constants/dimensions';


const styles = {
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  header: {
    width: '100%',
    height: '22.99270072992701%',
    borderBottomWidth: 3,
    borderBottomColor: 'black',
  },

  backButton: {
    position: 'absolute',
    // top: 22,
    // left: 24,
  },

  backButtonContainer: {
    width: 73,
    height: 83,
    justifyContent: 'center',
    alignItems: 'center',
  },

  backButtonIcon: {
    width: 25,
    height: 39,
  },

  shadow: {
    position: 'absolute',
    top: 57,
    left: 544,
    width: 96,
    height: 192,
  },

  illustration: {
    position: 'absolute',
    top: 9,
    left: 455,
    width: 128,
    height: 240,
  },

  title: {
    position: 'absolute',
    top: 183,
    left: 19,
    fontFamily: 'Galada-Regular',
    fontSize: 48,
    includeFontPadding: false,
    textAlignVertical: 'center',
    color: 'black',
  },

  form: {
    flex: 1,
    width: '100%',
    height: '77.00729927007299%',
    justifyContent: 'space-between',
  },

  inputsContainer: {
    flex: 1,
    paddingTop: 30,
    paddingLeft: 18,
    paddingRight: 18,
  },

  inputs: {
    width: '100%',
    height: 655,
    justifyContent: 'center',
  },

  fields: {
    height: 62 * 2 + 19,
    justifyContent: 'space-between',
  },

  textInput: {
    // marginTop: 19,
  },

  button: {
    width: '100%',
    height: 70,
    backgroundColor: '#f1c500ff',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

class LoginScreen extends Component {
  constructor(props) {
    super(props);

    this.fields = [ 'email', 'password' ];

    const { width, height } = props.window;
    this.state = {
      width,
      height,
      styles: createStyleSheet(styles, width, height),
      fieldsErrors: {},
    };
  }

  componentWillReceiveProps(nextProps) {
    const { width, height } = nextProps.window;
    this.setState({
      width,
      height,
      styles: createStyleSheet(styles, width, height),
    });
  }

  handleSubmit(data) {
    this.props.signinUser({ variables: { email: data.email, password: data.password } })
      .then((response) => {
        const token = response.data.signinUser.token;
        AsyncStorage.setItem('@GeraldStore:authToken', token);
        Alert.alert('Connexion réussie', 'Vous êtes connectés');
        this.props.unsubmitForm('connection');
      }).catch((e) => {
        Alert.alert('Erreur', 'Adresse e-mail ou mot de passe incorrect.');
        this.props.unsubmitForm('connection');
      });
  }

  validateFields() {
    const { forms } = this.props;
    const form = forms.connection || { data: {} };
    const formData = form.data;
    const errors = {};

    for (let i = 0; i < this.fields.length; i++) {
      const field = this.fields[i];
      const value = formData[field];
      if (value === undefined || value === '') {
        let error;
        if (field === 'gender') {
          error = 'Choisissez une option.';
        } else {
          error = 'Ce champs est requis, vous devez le remplir.';
        }
        errors[field] = error;
      }
    };

    if (errors['email'] === undefined && !this.validateEmail(formData['email'])) {
      errors['email'] = 'Adresse email invalide.';
    }

    if (errors['phone'] === undefined && !this.validatePhoneNumber(formData['phone'])) {
      errors['phone'] = 'Numéro de téléphone invalide.';
    }

    if (errors['confirmPassword'] === undefined && formData.password !== formData.confirmPassword) {
      errors['confirmPassword'] = 'Les mots de passe ne sont pas identiques.';
    }

    if (!formData.acceptCGU) {
      errors['acceptCGU'] = 'Vous devez accepter les CGU afin de vous inscrire.';
    }

    this.setState({ fieldsErrors: errors });

    return Object.keys(errors).length === 0;
  }

  validateEmail(email) {
    const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return regex.test(email);
  }

  render() {
    const { styles, width, height, fieldsErrors } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.backButton}>
            <TouchableWithoutFeedback onPress={() => this.props.navigation.goBack(null)}>
              <View style={styles.backButtonContainer}>
                <Image
                  style={styles.backButtonIcon}
                  source={require('./images/back-button.png')}
                  />
              </View>
            </TouchableWithoutFeedback>
          </View>

          <Image
            style={styles.shadow}
            source={require('./images/shadow.png')}
            />

          <Image
            style={styles.illustration}
            source={require('./images/gerald.png')}
            />

          <Text allowFontScaling={false} style={styles.title}>
            Bonjour ! Connectez-vous
          </Text>
        </View>

        <Form style={styles.form} name="connection" onSubmit={this.handleSubmit.bind(this)}>
          <View style={styles.inputsContainer}>
            <View style={styles.inputs}>
              <View style={styles.fields}>
                <TextInput name="email" style={styles.textInput} placeholder="Adresse e-mail" kayboardType="email-address" error={fieldsErrors['email']} />
                <TextInput name="password" secureTextEntry={true} style={styles.textInput} placeholder="Mot de passe" error={fieldsErrors['password']} />
              </View>
            </View>
          </View>

          <Button style={styles.button} value="C'est parti !" onPress={() => this.props.submitForm('connection')}/>
        </Form>
      </View>
    );
  }
}

const signinUser = gql`
  mutation ($email: String!, $password: String!) {
    signinUser(email: {email: $email, password: $password}) {
      token
    }
  }
`;

const userQuery = gql`
  query {
    user {
      id
    }
  }
`;

const mapStateToProps = state => ({
  window: state.window,
  forms: state.forms,
});

const container = connect(mapStateToProps, { submitForm, unsubmitForm })(LoginScreen);
export default graphql(userQuery, { options: { fetchPolicy: 'network-only' }})(
  graphql(signinUser, { name: 'signinUser' })(
    container
  )
);

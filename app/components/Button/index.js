import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableNativeFeedback,
  View,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { submitForm } from '../../actions';
import { createStyleSheet } from '../../functions/styles';


const styles = {
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },

  text: {
    fontFamily: 'Quicksand-Bold',
    fontSize: 26,
    color: '#ffffffff',
  },
};

class Button extends Component {
  constructor(props) {
    super(props);

    const { width, height } = props.window;
    this.state = {
      width,
      height,
      styles: createStyleSheet(styles, width, height),
    };
  }

  componentWillReceiveProps(nextProps) {
    const { width, height } = nextProps.window;
    this.setState({
      width,
      height,
      styles: createStyleSheet(styles, width, height)
    });
  }

  render() {
    const { styles } = this.state;
    const { onPress, value, style, forms, textStyle } = this.props;
    const form = forms[this.context.formName] || {};
    const { submitted } = form;

    return (
      <TouchableNativeFeedback onPress={submitted ? () => {} : onPress}>
        <View style={style}>
          <Text style={StyleSheet.flatten([ styles.text, textStyle ])}>
            { value }
          </Text>
        </View>
      </TouchableNativeFeedback>
    );
  }
}

Button.contextTypes = {
  formName: PropTypes.string,
};

const mapStateToProps = state => ({
  forms: state.forms,
  window: state.window,
});

export default connect(mapStateToProps, { submitForm })(Button);

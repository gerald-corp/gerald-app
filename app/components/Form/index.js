import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { updateForm, deleteForm } from '../../actions';


const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
  },
});

class Form extends Component {
  getChildContext() {
    return { formName: this.props.name };
  }

  componentWillReceiveProps(nextProps) {
    const { name, onSubmit, forms } = nextProps;
    const form = forms[name] || {};
    const oldForm = this.props.forms[name] || {};
    if (!oldForm.submitted && form.submitted) {
      onSubmit(form.data);
    }
  }

  componentWillMount() {
    this.props.updateForm(this.props.name, {});
  }

  componentWillUnmount() {
    this.props.deleteForm(this.props.name);
  }

  render() {
    return (
      <View style={this.props.style}>
        <KeyboardAwareScrollView contentContainerStyle={styles.contentContainer}>
          {this.props.children}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

Form.childContextTypes = {
  formName: PropTypes.string,
};

const mapStateToProps = state => ({
  forms: state.forms,
});

export default connect(mapStateToProps, { updateForm, deleteForm })(Form);

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import TextInputState from 'react-native/lib/TextInputState';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import FieldError from '../FieldError';

import { updateForm } from '../../actions';
import { createStyleSheet } from '../../functions/styles';
import { pickDate } from './functions/date';


const styles = {
  container: {
    justifyContent: 'center',
  },

  textInput: {
    height: 62,
    borderWidth: 1,
    borderColor: '#e0e0e0',
    paddingLeft: 12,
    paddingRight: 12,
    fontFamily: 'Quicksand-Bold',
    fontSize: 22,
    color: '#666666ff',
  },

  errorStyle: {
    borderColor: 'red',
  },

  errorContainer: {
    // top: 16.5,
  },
};

class DateInput extends Component {
  constructor(props) {
    super(props);

    const { width, height } = props.window;
    this.state = {
      width,
      height,
      styles: createStyleSheet(styles, width, height),
      date: "",
    };
  }

  componentWillReceiveProps(nextProps) {
    const { width, height } = nextProps.window;
    this.setState({
      width,
      height,
      styles: createStyleSheet(styles, width, height)
    });
  }

  dismissKeyboard() {
    TextInputState.blurTextInput(TextInputState.currentlyFocusedField());
  }

  async pickDate(initialDate) {
    console.log('pickdate');
    const { day, month, year } = await pickDate(initialDate);
    const date = `${day}/${month + 1}/${year}`;
    this.props.updateForm(this.context.formName, { [this.props.name]: date});
    this.setState({ date });
  }

  render() {
    const { error, style } = this.props;
    const { styles, text } = this.state;
    const { submitted } = this.props.forms[this.context.formName];
    const combinedStyles = error === undefined ? [ styles.textInput ]
          : [ styles.textInput, styles.errorStyle ];

    const errorComponent = <FieldError error={error} style={styles.errorContainer} />;

    return (
      <View style={StyleSheet.flatten([ styles.container, style ])}>
        <TextInput
          style={StyleSheet.flatten(combinedStyles)}
          placeholder={this.props.placeholder}
          underlineColorAndroid="rgba(0, 0, 0, 0)"
          value={this.state.date}
          onFocus={() => { this.dismissKeyboard(); if (!submitted) this.pickDate(new Date()); }}
          />

          { error === undefined ? null : errorComponent }
      </View>
    );
  }
}

// onFocus={() => { this.dismissKeyboard(); this.pickDate(new Date()); }}
DateInput.contextTypes = {
  formName: PropTypes.string,
};

const mapStateToProps = state => ({
  forms: state.forms,
  window: state.window,
});

export default connect(mapStateToProps, { updateForm })(DateInput);

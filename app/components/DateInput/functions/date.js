import { DatePickerAndroid } from 'react-native';

export async function pickDate(date) {
  try {
    const { action, day, month, year } = await DatePickerAndroid.open({ date });
    if (action !== DatePickerAndroid.dismissedAction) {
      return { day, month, year };
    }
  } catch ({code, message}) {
    console.warn('Cannot open date picker', message);
  }
}

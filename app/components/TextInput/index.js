import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import FieldError from '../FieldError';

import { updateForm } from '../../actions';
import { createStyleSheet } from '../../functions/styles';
import { capitalizeWords } from '../../functions/strings';


const styles = {
  container: {
    // justifyContent: 'center',
  },

  textInput: {
    height: 62,
    borderWidth: 1,
    borderColor: '#e0e0e0',
    paddingLeft: 12,
    paddingRight: 12,
    fontFamily: 'Quicksand-Bold',
    fontSize: 22,
    color: '#666666ff',
  },

  errorStyle: {
    borderColor: 'red',
  },

  errorContainer: {
    // top: 16.5,
  },
};

class GTextInput extends Component {
  constructor(props) {
    super(props);

    const { width, height } = props.window;
    this.state = {
      width,
      height,
      styles: createStyleSheet(styles, width, height),
      text: "",
    };
  }

  componentWillReceiveProps(nextProps) {
    const { width, height } = nextProps.window;
    this.setState({
      width,
      height,
      styles: createStyleSheet(styles, width, height)
    });
  }

  handleChange(text) {
    let newText = text;
    if (this.props.autoCapitalize === 'words') {
      newText = capitalizeWords(text);
    }
    this.props.updateForm(this.context.formName, { [this.props.name]: newText });
    this.setState({ text: newText });
  }

  render() {
    const { forms, error, style, autoCorrect } = this.props;
    const { styles, text } = this.state;
    const { submitted } = forms[this.context.formName];
    const combinedStyles = error === undefined ? [ styles.textInput ]
          : [ styles.textInput, styles.errorStyle ];

    const errorComponent = <FieldError error={error} style={styles.errorContainer} />;

    return (
      <View style={StyleSheet.flatten([ styles.container, style ])}>
        <TextInput
          {...this.props}
          editable={!submitted}
          autoCorrect={autoCorrect || false}
          style={StyleSheet.flatten(combinedStyles)}
          underlineColorAndroid="rgba(0, 0, 0, 0)"
          onChangeText={this.handleChange.bind(this)}
          value={text}
          />

        { error === undefined ? null : errorComponent }
      </View>
    );
  }
}

GTextInput.contextTypes = {
  formName: PropTypes.string,
};

const mapStateToProps = state => ({
  forms: state.forms,
  window: state.window,
});

export default connect(mapStateToProps, { updateForm })(GTextInput);

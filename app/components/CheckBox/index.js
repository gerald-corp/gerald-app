import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import FieldError from '../FieldError';

import { updateForm } from '../../actions';
import { createStyleSheet } from '../../functions/styles';


const styles = {
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  checkbox: {
    borderWidth: 1,
    borderColor: '#e0e0e0',
    width: 34,
    height_h: 34,
    alignItems: 'center',
    justifyContent: 'center',
  },

  check: {
    width: 22,
    height: 14,
  },

  children: {
    marginLeft: 9,
  },

  errorContainer: {
    // top: 
  },
};

class CheckBox extends Component {
  constructor(props) {
    super(props);

    const { width, height } = props.window;
    this.state = {
      width,
      height,
      styles: createStyleSheet(styles, width, height),
      checked: props.checked || false,
    };
  }

  componentWillReceiveProps(nextProps) {
    const { width, height } = nextProps.window;
    this.setState({
      width,
      height,
      styles: createStyleSheet(styles, width, height)
    });
  }

  handlePress() {
    const checked = !this.state.checked;
    this.props.updateForm(this.context.formName, { [this.props.name]: checked });
    this.setState({ checked });
  }

  render() {
    const { checked, styles } = this.state;
    const { error, style, forms } = this.props;
    const { submitted } = forms[this.context.formName];

    const errorComponent = <FieldError error={error} style={styles.errorContainer}/>;

    return (
      <View style={style}>
        <TouchableWithoutFeedback onPress={submitted ? () => {} : this.handlePress.bind(this)}>
          <View style={styles.container}>
            <View style={styles.checkbox}>
              {checked ? <Image source={require('./images/check.png')} style={styles.check} /> : null}
            </View>
            <View style={styles.children}>
              {this.props.children}
            </View>
          </View>
        </TouchableWithoutFeedback>

        { error === undefined ? null : errorComponent }
      </View>
    );
  }
}

CheckBox.contextTypes = {
  formName: PropTypes.string,
};

const mapStateToProps = state => ({
  forms: state.forms,
  window: state.window,
});

export default connect(mapStateToProps, { updateForm })(CheckBox);

import React, { Component } from 'react';
import {
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { connect } from 'react-redux';

import { createStyleSheet } from '../../functions/styles.js';


const styles = {
  container: {
    flex: 1,
  },

  content: {
    flex: 1,
    flexDirection: 'row',
    // justifyContent: 'space-between',
  },

  circle: {
    width: 34,
    height_h: 34,
    borderWidth: 1.2,
    borderRadius: 17,
    borderColor: '#e0e0e0',
    alignItems: 'center',
    justifyContent: 'center',
  },

  dot: {
    width: 8,
    height_h: 8,
    borderRadius: 4,
    backgroundColor: 'black',
  },

  text: {
    alignSelf: 'center',
    marginLeft: 9,
    includeFontPadding: false,
    textAlignVertical: 'center',
    fontFamily: 'Quicksand-Bold',
    fontSize: 22,
    color: '#666666ff',
  },
};

class RadioButton extends Component {
  constructor(props) {
    super(props);

    const { width, height } = props.window;
    this.state = {
      width,
      height,
      styles: createStyleSheet(styles, width, height)
    };
  }

  componentWillReceiveProps(nextProps) {
    const { width, height } = nextProps.window;
    this.setState({
      width,
      height,
      styles: createStyleSheet(styles, width, height)
    });
  }

  render() {
    const { id, value, text, selected, onPress } = this.props;
    const { styles } = this.state;

    return (
      <TouchableWithoutFeedback style={styles.container} onPress={() => onPress(id, value)}>
        <View style={styles.content}>
          <View style={styles.circle}>
            { selected ? <View style={styles.dot}></View> : null }
          </View>

          <Text allowFontScaling={false} style={styles.text}>
            { text }
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = state => ({
  window: state.window,
});

export default connect(mapStateToProps)(RadioButton);

import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import FieldError from '../FieldError';

import { createStyleSheet } from '../../functions/styles';
import { updateForm } from '../../actions';


const styles = {
  container: {
    justifyContent: 'center',
  },

  errorContainer: {
    top: 1,
  },
};

class RadioButtonsGroup extends Component {
  constructor(props) {
    super(props);
    this.childrenCount = React.Children.count(props.children);

    const { width, height } = props.window;
    this.state = {
      width,
      height,
      styles: createStyleSheet(styles, width, height),
      selected: new Array(this.childrenCount).fill(false),
      value: "",
    };
  }

  componentWillReceiveProps(nextProps) {
    const { width, height } = nextProps.window;
    this.setState({
      width,
      height,
      styles: createStyleSheet(styles, width, height)
    });
  }

  handlePress(id, value) {
    const nextSelected = new Array(this.childrenCount).fill(false);
    nextSelected[id] = true;

    const kv = {};
    kv[this.props.name] = value;
    this.props.updateForm(this.context.formName, kv);
    this.setState({
      selected: nextSelected,
      value,
    });
  }

  render() {
    const { children, error } = this.props;
    const { styles } = this.state;
    const { submitted } = this.props.forms[this.context.formName];

    const errorComponent = <FieldError error={error} style={styles.errorContainer} />;

    return (
      <View style={styles.container}>
        <View style={this.props.style}>
          {React.Children.toArray(children).map((child, index) => (
            React.cloneElement(
              child,
              {
                id: index,
                selected: this.state.selected[index],
                onPress: submitted ? () => {} : this.handlePress.bind(this),
              }
            )
          ))}
        </View>

        { error === undefined ? null : errorComponent }
      </View>
    );
  }
}

RadioButtonsGroup.contextTypes = {
  formName: PropTypes.string,
};

const mapStateToProps = state => ({
  window: state.window,
  forms: state.forms,
});

export default connect(mapStateToProps, { updateForm })(RadioButtonsGroup);

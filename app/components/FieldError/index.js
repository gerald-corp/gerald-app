import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { connect } from 'react-redux';

import { createStyleSheet } from '../../functions/styles';


const styles = {
  container: {
    position: 'absolute',
    // left: '93%',
    // alignSelf: 'flex-end',
    top: 0,
    right: 10,
    height: '100%',
  },

  button: {
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center',
    left: 10,
    width: 50,
    height: '100%',
  },

  buttonContainer: {
    width: 50,
    // height: 62,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  buttonTextContainer: {
    // width: '100%',
    // height: '100%',
    width: 30,
    height_h: 30,
    borderRadius: 15,
    backgroundColor: '#bc2126',
    justifyContent: 'center',
    alignItems: 'center',
  },

  buttonText: {
    fontFamily: 'Quicksand-Bold',
    fontSize: 25,
    color: 'white',
  },

  errorContainer: {
    // marginTop: 6,
    bottom: '50%',
    transform: [{ translateY: 20 }],
    zIndex: 10,
  },

  arrowContainer: {
    // alignSelf: 'flex-end',
    // justifyContent: 'center',
    // alignItems: 'center',
    // width: Math.sqrt(Math.pow(15, 2) * 2),
    // height_h: Math.sqrt(Math.pow(15, 2) * 2),
  },

  errorContainerArrow: {
    // // backgroundColor: '#777575',
    // backgroundColor: '#bc2126',
    // width: 15,
    // height_h: 15,
    // transform: [{ rotate: '45deg' }],
  },

  errorMessageContainer: {
    // top: -(Math.sqrt(Math.pow(15, 2) * 2) / 2),
    padding: 6,
    backgroundColor: '#777575',
  },

  errorText: {
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
    color: 'white',
  },
};

class FieldError extends Component {
  constructor(props) {
    super(props);

    const { width, height } = props.window;
    this.state = {
      width,
      height,
      styles: createStyleSheet(styles, width, height),
      errorDisplay: 'none',
    };
  }

  componentWillReceiveProps(nextProps) {
    const { width, height } = nextProps.window;
    this.setState({
      width,
      height,
      styles: createStyleSheet(styles, width, height)
    });
  }

  toggleErrorMessage() {
    const errorDisplay = this.state.errorDisplay === 'flex' ? 'none' : 'flex';
    this.setState({ errorDisplay });
  }

  render() {
    const { styles, errorDisplay } = this.state;
    const { error, style } = this.props;

    return (
      <View style={StyleSheet.flatten([ styles.container, style ])}>
        <View style={styles.button}>
          <TouchableWithoutFeedback style={styles.buttonTextContainer} onPress={this.toggleErrorMessage.bind(this)}>
            <View style={styles.buttonContainer}>
              <View style={styles.buttonTextContainer}>
                <Text style={styles.buttonText}>!</Text>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </View>

        <View style={StyleSheet.flatten([ styles.errorContainer, { display: errorDisplay } ])}>
          <View style={styles.arrowContainer}>
            <View style={styles.errorContainerArrow} />
          </View>

          <View style={styles.errorMessageContainer}>
            <Text style={styles.errorText}>{ error }</Text>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  window: state.window,
});

export default connect(mapStateToProps)(FieldError);

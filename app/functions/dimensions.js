import { PixelRatio } from 'react-native';

export function getFontSize(sp, diagonalRatio) {
  return PixelRatio.roundToNearestPixel(sp * diagonalRatio);
}

export function getHSize(dp, widthRatio) {
  return dp * widthRatio;
}

export function getVSize(dp, heightRatio) {
  return dp * heightRatio;
}

export function getSize(dp, ratio) {
  return dp * ratio;
}

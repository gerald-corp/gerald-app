export function capitalizeWords(str) {
  return str.replace(/\b[a-z]/g, (f) => f.toUpperCase());
}

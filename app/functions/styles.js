import { PixelRatio, StyleSheet } from 'react-native';
import { getSize, getFontSize } from '../functions/dimensions';
import {
  PSD_WIDTH,
  PSD_HEIGHT,
  PSD_DIAGONAL,
  WINDOW_WIDTH,
  WINDOW_HEIGHT,
} from '../constants/dimensions';


export function createStyleSheet(styles, width, height) {
  const widthRatio = width / PSD_WIDTH;
  const heightRatio = height / PSD_HEIGHT;
  const diagonal = Math.sqrt((width*width) + (height*height));
  const diagonalRatio = diagonal / PSD_DIAGONAL;
  const newStyles = {};
  const hmeasures = [
    'width', 'minWidth', 'maxWidth',
    'left', 'right',
    'marginLeft', 'marginRight',
    'paddingLeft', 'paddingRight',
    'borderRadius', 'borderWidth', 'borderTopWidth', 'borderRightWidth', 'borderBottomWidth', 'borderLeftWidth',
    'translateX',
  ];
  const vmeasures = [
    'height', 'minHeight', 'maxHeight',
    'top', 'bottom',
    'marginTop', 'marginBottom',
    'paddingTop', 'paddingBottom',
    'lineHeight',
    'translateY',
  ];
  const fmeasures = [
    'fontSize',
  ];

  for (let styleKey in styles) {
    const style = styles[styleKey];
    const newStyle = {};

    for (let key in style) {
      let value = style[key];
      let _width = width;
      let _height = height;
      let ratio_h = widthRatio;
      let ratio_v = heightRatio;
      let ratio;

      if (/.+_h$/.test(key)) {
        key = key.replace(/_h$/, '');
        ratio_v = widthRatio;
        _height = width;
      } else if (/.+_v$/.test(key)) {
        key = key.replace(/_v$/, '');
        ratio_h = heightRatio;
        _width = height;
      }

      if (typeof value === 'number') {
        if (hmeasures.includes(key)) {
          ratio = ratio_h;
        } else if (vmeasures.includes(key)) {
          ratio = ratio_v;
        } else if (fmeasures.includes(key)) {
          ratio = diagonalRatio;
        } else {
          ratio = 1;
        }

        if (key === 'width' && value === WINDOW_WIDTH) {
          value = _width;
          ratio = 1;
        } else if (key === 'height' && value === WINDOW_HEIGHT) {
          value = _height;
          ratio = 1;
        }

        if (key === 'fontSize') {
          newStyle[key] = getFontSize(value, ratio);
        } else {
          newStyle[key] = getSize(value, ratio);
        }
      } else if (typeof value === 'object' && value.length !== undefined) {
        if (key === 'transform') {
          const transform = [];

          for (let i = 0; i < value.length; i++) {
            const t = Object.keys(value[i]).map((k) => {
              if (hmeasures.includes(k)) {
                return { [k]: getSize(value[i][k], ratio_h) };
              } else if (vmeasures.includes(k)) {
                return { [k]: getSize(value[i][k], ratio_v) };
              } else {
                return { [k]: value[i][k] };
              }
            });

            transform.push(t[0]);
          }

          newStyle[key] = transform;
        } else {
          newStyle[key] = value;
        }
      } else {
        newStyle[key] = value;
      }
    }

    newStyles[styleKey] = newStyle;
  }

  return StyleSheet.create(newStyles);
}

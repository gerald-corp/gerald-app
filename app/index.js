import React, { Component } from 'react';
import { AsyncStorage, StyleSheet, View } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { createStore } from 'redux';
import { connect, Provider } from 'react-redux';
import { ApolloProvider, createNetworkInterface, ApolloClient } from 'react-apollo';

import HomeScreen from './screens/HomeScreen';
import RegistrationScreen from './screens/RegistrationScreen';
import LoginScreen from './screens/LoginScreen';
import reducer from './reducers';
import { resizeWindow } from './actions';


const store = createStore(reducer);

const networkInterface = createNetworkInterface({ uri: 'https://api.graph.cool/simple/v1/cj6v4dnuz002d01276v8si3mj' });
networkInterface.use([{
  async applyMiddleware (req, next) {
    if (!req.options.headers) {
      req.options.headers = {};
    }

    const authToken = await AsyncStorage.getItem('@GeraldStore:authToken');
    if (authToken !== null) {
      req.options.headers.authorization = `Bearer ${authToken}`;
    }
    next();
  },
}]);
const client = new ApolloClient({ networkInterface });

const styles = StyleSheet.create({
  appContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
  },
});

const Navigator = StackNavigator({
  Home: {
    screen: HomeScreen,
  },
  Registration: {
    path: 'registration',
    screen: RegistrationScreen,
  },
  Login: {
    path: 'login',
    screen: LoginScreen,
  },
}, { headerMode: 'none' });

const App = ({ resizeWindow, window }) => (
  <View
    style={styles.appContainer}
    onLayout={(event) => {
      let { width, height } = event.nativeEvent.layout;
      resizeWindow(width, height);
    }}
    >
    <Navigator style={{ width: window.width, height: window.height }} />
  </View>
);

const mapStateToProps = state => ({
  window: state.window,
});

const AppContainer = connect(
  mapStateToProps,
  { resizeWindow }
)(App);

const GeraldApp = () => (
  <Provider store={store}>
    <ApolloProvider client={client}>
      <AppContainer />
    </ApolloProvider>
  </Provider>
);

export default GeraldApp;
